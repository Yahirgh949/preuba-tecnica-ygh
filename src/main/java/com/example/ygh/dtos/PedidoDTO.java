package com.example.ygh.dtos;

import java.util.Date;

public class PedidoDTO {
    private Integer id;
    private double total;
    private Date dateSale;
    private DetallePedidoDTO detallePedidoDTO;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public Date getDateSale() {
        return dateSale;
    }

    public void setDateSale(Date dateSale) {
        this.dateSale = dateSale;
    }

    public DetallePedidoDTO getDetallePedidoDTO() {
        return detallePedidoDTO;
    }

    public void setDetallePedidoDTO(DetallePedidoDTO detallePedidoDTO) {
        this.detallePedidoDTO = detallePedidoDTO;
    }
}
