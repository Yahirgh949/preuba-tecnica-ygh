package com.example.ygh.controllers.creates;

import com.example.ygh.dtos.PedidoDTO;
import com.example.ygh.entities.Pedido;
import com.example.ygh.service.create.PedidoCreateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/pedido")
public class PedidoCreateController {

    private PedidoCreateService pedidoCreateService;

    @Autowired
    public void setPedidoCreateService(PedidoCreateService pedidoCreateService) {
        this.pedidoCreateService = pedidoCreateService;
    }

    @PostMapping()
    public Pedido save(@RequestBody() PedidoDTO pedido) {
       return this.pedidoCreateService.saveDTO(pedido);
    }
}
