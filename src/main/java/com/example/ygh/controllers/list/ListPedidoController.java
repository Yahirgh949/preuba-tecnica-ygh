package com.example.ygh.controllers.list;

import com.example.ygh.entities.Pedido;
import com.example.ygh.service.list.PedidoListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/pedidos")
public class ListPedidoController {

    private PedidoListService pedidoListService;

    @Autowired
    public void setPedidoListService(PedidoListService pedidoListService) {
        this.pedidoListService = pedidoListService;
    }

    @GetMapping()
    public List<Pedido> list() {
        return this.pedidoListService.list();
    }
}
