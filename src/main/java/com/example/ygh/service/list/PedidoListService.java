package com.example.ygh.service.list;

import com.example.ygh.entities.Pedido;

import java.util.List;

public interface PedidoListService {
    List<Pedido> list();
}
