package com.example.ygh.service.list.impl;

import com.example.ygh.entities.DetallePedido;
import com.example.ygh.repositories.DetallePedidoRepository;
import com.example.ygh.service.list.DetallePedidoListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DetallePedidoListServiceImpl implements DetallePedidoListService {

    private DetallePedidoRepository detallePedidoRepository;

    @Autowired
    public void setDetallePedidoRepository(DetallePedidoRepository detallePedidoRepository) {
        this.detallePedidoRepository = detallePedidoRepository;
    }

    public List<DetallePedido> getDetallePedido() {
        return detallePedidoRepository.findAll();
    }
}
