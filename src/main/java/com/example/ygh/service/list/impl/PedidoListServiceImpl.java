package com.example.ygh.service.list.impl;

import com.example.ygh.entities.Pedido;
import com.example.ygh.repositories.PedidoRepository;
import com.example.ygh.service.list.PedidoListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PedidoListServiceImpl implements PedidoListService {

    private PedidoRepository pedidoRepository;

    @Autowired
    public void setPedidoRepository(PedidoRepository pedidoRepository) {
        this.pedidoRepository = pedidoRepository;
    }

    @Override
    public List<Pedido> list() {
        return this.pedidoRepository.findAll();
    }
}
