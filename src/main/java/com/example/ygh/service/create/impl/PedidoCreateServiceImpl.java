package com.example.ygh.service.create.impl;

import com.example.ygh.dtos.PedidoDTO;
import com.example.ygh.entities.DetallePedido;
import com.example.ygh.entities.Pedido;
import com.example.ygh.repositories.DetallePedidoRepository;
import com.example.ygh.repositories.PedidoRepository;
import com.example.ygh.service.create.PedidoCreateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Date;

@Service
public class PedidoCreateServiceImpl implements PedidoCreateService {

    private PedidoRepository pedidoRepository;
    private DetallePedidoRepository detallePedidoRepository;

    @Autowired
    public void setPedidoRepository(PedidoRepository pedidoRepository) {
        this.pedidoRepository = pedidoRepository;
    }

    @Autowired
    public void setDetallePedidoRepository(DetallePedidoRepository detallePedidoRepository) {
        this.detallePedidoRepository = detallePedidoRepository;
    }

    @Override
    public Pedido save(@RequestBody() Pedido pedido) {
        return pedido = this.pedidoRepository.save(pedido);
    }

    @Override
    public Pedido saveDTO(PedidoDTO pedido) {
        Pedido pedido1 = new Pedido();

        pedido1.setDateSale(new Date());
        pedido1.setTotal(pedido.getTotal());
        pedido1 = this.pedidoRepository.save(pedido1);

        if(pedido.getDetallePedidoDTO() != null) {
            DetallePedido detallePedido = new DetallePedido();
            detallePedido.setAmount(pedido.getDetallePedidoDTO().getAmount());
            detallePedido.setIdPedido(pedido1);
            detallePedido.setSku(pedido.getDetallePedidoDTO().getSku());
            detallePedido.setPrice(pedido.getDetallePedidoDTO().getPrice());
            detallePedido = this.detallePedidoRepository.save(detallePedido);
        }

        return pedido1;

    }
}
