package com.example.ygh.service.create;

import com.example.ygh.dtos.PedidoDTO;
import com.example.ygh.entities.Pedido;

public interface PedidoCreateService {
    Pedido save(Pedido pedido);
    Pedido saveDTO(PedidoDTO pedido);
}
