package com.example.ygh.entities;

import javax.persistence.*;

@Entity
@Table(name = "PEDIDOS_DETALLE_W")
public class DetallePedido {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne
    @JoinColumn(name = "ID_PEDIDO", referencedColumnName = "ID")
    private Pedido idPedido;

    @Column(name = "SKU", length = 150)
    private String sku;

    @Column(name = "AMOUT")
    private double amount;

    @Column(name = "PRICE")
    private  double price;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Pedido getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(Pedido idPedido) {
        this.idPedido = idPedido;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
