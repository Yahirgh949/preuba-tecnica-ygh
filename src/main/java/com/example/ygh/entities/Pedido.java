package com.example.ygh.entities;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "PEDIDOS_W")
public class Pedido {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "TOTAL", precision=10, scale=2)
    private double total;

    @Column(name = "DATE_SALE")
    private Date dateSale;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public Date getDateSale() {
        return dateSale;
    }

    public void setDateSale(Date dateSale) {
        this.dateSale = dateSale;
    }
}
